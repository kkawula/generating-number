package com.thinksofthenumbers.thinksofthenumbers;

import java.util.Random;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
public class GeneratingNumberService {

    @Value("${number-range.min}")
    private int low;
    @Value("${number-range.max}")
    private int high;
    int result;

    public Integer checkNumber(final Integer numberToCheck) {
        if (argumentCheck(numberToCheck)) {
            if (numberToCheck == result) {
                return 0;
            } else if (numberToCheck < result) {
                return -1;
            } else {
                return 1;
            }
        } else {
            throw new IllegalArgumentException("Number to check must be between " + low + " and " + high);
        }
    }

    @PostConstruct
    public void init() {
        Random r = new Random();
        result = r.nextInt(high-low) + low;
        log.info("Generated value is: " + result);
    }

    private boolean argumentCheck(final Integer numberToCheck) {
        return numberToCheck >= low && numberToCheck <= high;
    }
}
