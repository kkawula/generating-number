package com.thinksofthenumbers.thinksofthenumbers.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;

public class AuthenticationToken extends AbstractAuthenticationToken {

    public static final String DEFAULT_TOKEN = "85843e8f-3ae4-46f0-831f-cfb3c4025c33";

    private final String token;

    public AuthenticationToken(String token) {
        super(null);

        this.token = token;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return getToken();
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    public String getToken() {
        return token;
    }
}
