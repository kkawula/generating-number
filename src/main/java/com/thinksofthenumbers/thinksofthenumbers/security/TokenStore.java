package com.thinksofthenumbers.thinksofthenumbers.security;

import java.util.HashSet;
import java.util.Set;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class TokenStore {

    @Value("#{'${security.tokens}'.split(',')}")
    private Set<String> tokens;

    @PostConstruct
    public void initializeStore() {
        if (tokens == null || tokens.isEmpty()) {
            log.warn("No configured API tokens, using default one: {}", AuthenticationToken.DEFAULT_TOKEN);
            tokens = new HashSet<>();
            tokens.add(AuthenticationToken.DEFAULT_TOKEN);

            return;
        }

        log.info("Configured API tokens: {}", tokens);
    }

    public boolean containsToken(String token) {
        if (tokens == null) {
            return false;
        }

        return tokens.contains(token);
    }
}
