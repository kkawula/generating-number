package com.thinksofthenumbers.thinksofthenumbers.security;

import java.util.Objects;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

@Log4j2
public class AuthProvider implements AuthenticationProvider {

    @Autowired
    private TokenStore tokenStore;

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        final AuthenticationToken tokenContainer = (AuthenticationToken) auth;
        final String token = tokenContainer.getToken();

        if (Objects.isNull(token) || token.isEmpty() || !tokenStore.containsToken(token)) {
            throw new BadCredentialsException("Invalid token");
        }

        return new AuthenticationToken(token);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return AuthenticationToken.class.isAssignableFrom(authentication);
    }
}
