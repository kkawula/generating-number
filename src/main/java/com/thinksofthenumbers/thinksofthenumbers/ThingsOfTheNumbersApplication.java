package com.thinksofthenumbers.thinksofthenumbers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThingsOfTheNumbersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThingsOfTheNumbersApplication.class, args);
	}

}
