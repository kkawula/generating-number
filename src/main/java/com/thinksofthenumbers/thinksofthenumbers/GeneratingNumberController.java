package com.thinksofthenumbers.thinksofthenumbers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class GeneratingNumberController {

    private final GeneratingNumberService generatingNumberService;

    @GetMapping("/number/{number}")
    public Integer getListOfStamps(@PathVariable Integer number) {
        return generatingNumberService.checkNumber(number);
    }

}
